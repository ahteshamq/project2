const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' }; // use this object to test your functions
const keyF = require("./keys.cjs")
function pairs(obj) {
        var keysArr = keyF(obj);
        var length = keysArr.length;
        var pair = [0*length]
        for (var i = 0; i < length; i++) {
          pair[i] = [keysArr[i], obj[keysArr[i]]];
        }
        return pair;
      
    // Convert an object into a list of [key, value] pairs.
    // http://underscorejs.org/#pairs
}

module.exports = pairs;
