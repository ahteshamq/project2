
const keyFunct = require("./keys.cjs")
function mapObject(obj, cb) {
    const arr = keyFunct(obj)
    if (arr.length===0){
        return {}
    }
    else{
        for(var i = 0;i<arr.length;i++){
            obj[arr[i]]=cb(obj[arr[i]],5)
        }
        return obj
    }

    // Like map for arrays, but for objects. Transform the value of each property in turn by passing it to the callback function.
    // http://underscorejs.org/#mapObject
}
function callBack(val,key){
    return val+key
}
module.exports = {
    mapObject: mapObject,
    callBack: callBack
}
