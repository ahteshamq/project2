const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' }; // use this object to test your functions


function keys(obj) {
    if(obj
    && Object.keys(obj).length === 0){
        return []
    }
    if(typeof obj === 'object'){
        const arr = []
        for (let key in obj){
            arr.push(key)
        }
        return arr
    }
    return []
    // Retrieve all the names of the object's properties.
    // Return the keys as strings in an array.
    // Based on http://underscorejs.org/#keys
}
// const arr = keys({})
// console.log(arr)
module.exports = keys;