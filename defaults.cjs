const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' }; // use this object to test your functions
const newProp = {name: 'Bruce Wayne', age: 36, location: 'Gotham', nickname: 'Batman', universe: 'DC'}

const keyF = require("./keys.cjs")


function defaults(obj, defaultProps) {
    var objArr = keyF(obj)
    var dPropArr = keyF(defaultProps)
    if(objArr.length===0 && dPropArr.length===0){
        return {}
    }
    else{
        for(var i = 0;i<dPropArr.length;i++){
            for(var j =0;j<objArr.length;j++){
                if(dPropArr[i]!=objArr[j]){
                    obj[dPropArr[i]]=defaultProps[dPropArr[i]]
                }
            }
        }
        return obj
    }
}
module.exports = defaults;

