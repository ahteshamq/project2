const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' }; // use this object to test your functions

function values(obj) {

    if(obj && Object.keys(obj).length == 0){
            return []
    }
    if(typeof obj === 'object'){
        const arr = []
        for (let value in obj){
            arr.push(obj[value])
        }
        return arr
    }
    return []
    // Return all of the values of the object's own properties.
    // Ignore functions
    // http://underscorejs.org/#values
}
module.exports = values;