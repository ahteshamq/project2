const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' }; // use this object to test your functions
const keyFunct = require("./keys.cjs")
function invert(obj) {
        var res = {};
        var keyArr = keyFunct(obj);
        for (var i = 0, length = keyArr.length; i < length; i++) {
          res[obj[keyArr[i]]] = keyArr[i];
        }
        return res;
      
    // Returns a copy of the object where the keys have become the values and the values the keys.
    // Assume that all of the object's values will be unique and string serializable.
    // http://underscorejs.org/#invert
}

module.exports = invert;